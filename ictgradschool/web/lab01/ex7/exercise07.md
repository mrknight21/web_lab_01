Herbivorous-
* Horse
* Cow
* Sheep
* Capybara

Carnivorous-
* Polar Bear
* Frog
* Hawk
* Lion

Omnivorous-
* Cat
* Dog
* Rat
* Fox
* Raccoon
* Chicken